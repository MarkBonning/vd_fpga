-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  digital_outputs.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  30/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the digital_outputs of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity digital_outputs is
    port (
    	
    	clock20mhz_i							: in std_logic;							-- system clock
    	reset_ni									: in std_logic;							-- system reset	

		gp_o0_axle_1_wsp_in_hold_o			: out std_logic;						-- FPGA Ouput pin
		gp_o1_axle_1_wsp_in_vent_o			: out std_logic;						-- FPGA Ouput pin
		gp_o2_axle_2_wsp_in_hold_o			: out std_logic;						-- FPGA Ouput pin
		gp_o3_axle_2_wsp_in_vent_o			: out std_logic;						-- FPGA Ouput pin
		gp_o4_wsp_in_hold_o					: out std_logic;						-- FPGA Ouput pin
		gp_o5_wsp_in_vent_o					: out std_logic;						-- FPGA Ouput pin
		
		gp_o0_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.
		gp_o1_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.
		gp_o2_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.
		gp_o3_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.
		gp_o4_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.
		gp_o5_wsp_select_i					: in std_logic;							-- Microcontroller Application configuration.


		data_reg_i								: in std_logic_vector(5 downto 0);		-- Microcontroller Application configuration.
		
		wsp_axle_1_hold_i						: in std_logic;							-- Command from Microprocessor
		wsp_axle_1_vent_i						: in std_logic;							-- Command from Microprocessor
		axle_1_timer_enable_i				: in std_logic;							-- Internal FPGA signal
		wsp_axle_2_hold_i						: in std_logic;							-- Command from Microprocessor
		wsp_axle_2_vent_i						: in std_logic;							-- Command from Microprocessor
		axle_2_Timer_Enable_i				: in std_logic;							-- Internal FPGA signal
		internal_lbsr_vd_fpga_i				: in std_logic;							-- low BSR transducer
		external_lbsr_trip_vd_fpga_i		: in std_logic;							-- low BSR transducer
				  
		external_lbsr_enabled_i				: in std_logic;							-- Microcontroller Application configuration. 
		internal_lbsr_enabled_i				: in std_logic							-- Microcontroller Application configuration. 
		
    );
end entity digital_outputs;

architecture imp_digital_outputs of digital_outputs is

	-- local signals
	signal low_bsr_s						: std_logic;							-- Low BSR signal																					
	

begin

-- logic

low_bsr_s	<= (external_lbsr_enabled_i and external_lbsr_trip_vd_fpga_i) or (internal_lbsr_enabled_i and internal_lbsr_vd_fpga_i);
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- control digital outputs
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	digital_outputs : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			
			gp_o0_axle_1_wsp_in_hold_o		<= '0';
			gp_o1_axle_1_wsp_in_vent_o		<= '0';
			gp_o2_axle_2_wsp_in_hold_o		<= '0';
			gp_o3_axle_2_wsp_in_vent_o		<= '0';
			gp_o4_wsp_in_hold_o				<= '0';
			gp_o5_wsp_in_vent_o				<= '0';			
		
		elsif rising_edge (clock20mhz_i) then

			-- if the config bit is clear, then the digital outputs follow the data register else it follows configuation logic

			if 	gp_o0_wsp_select_i = '0' then	gp_o0_axle_1_wsp_in_hold_o		<= data_reg_i(0);
			else	gp_o0_axle_1_wsp_in_hold_o <= axle_1_timer_enable_i and wsp_axle_1_hold_i and not low_bsr_s;
			end if;
			
			if 	gp_o1_wsp_select_i = '0' then	gp_o1_axle_1_wsp_in_vent_o		<= data_reg_i(1);
			else	gp_o1_axle_1_wsp_in_vent_o <= axle_1_timer_enable_i and wsp_axle_1_vent_i and not low_bsr_s;
			end if;
		
			if 	gp_o2_wsp_select_i = '0' then	gp_o2_axle_2_wsp_in_hold_o		<= data_reg_i(2);
			else	gp_o2_axle_2_wsp_in_hold_o <= axle_2_Timer_Enable_i and wsp_axle_2_hold_i and not low_bsr_s;
			end if;
		
			
			if 	gp_o3_wsp_select_i = '0' then	gp_o3_axle_2_wsp_in_vent_o		<= data_reg_i(3);
			else	gp_o3_axle_2_wsp_in_vent_o <= axle_2_Timer_Enable_i and wsp_axle_2_vent_i and not low_bsr_s;
			end if;

			if 	gp_o4_wsp_select_i = '0' then 	gp_o4_wsp_in_hold_o	<= data_reg_i(4);
			else	gp_o4_wsp_in_hold_o <= 	((axle_1_timer_enable_i and wsp_axle_1_hold_i) or (axle_2_Timer_Enable_i and wsp_axle_2_hold_i)) and not low_bsr_s;
			end if;
				
			if 	gp_o5_wsp_select_i = '0' then 	gp_o5_wsp_in_vent_o	<= data_reg_i(5);			
			else	gp_o5_wsp_in_vent_o  <= ((axle_1_timer_enable_i and wsp_axle_1_vent_i) or (axle_2_Timer_Enable_i and wsp_axle_2_vent_i)) and not low_bsr_s;
			end if;
	
	

		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_digital_outputs;

