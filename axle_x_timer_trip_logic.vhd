-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_timer_trip_logic.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  30/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the axle X timer trip logic of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_timer_trip is
    port (

		Axle_X_Hold_timer_trip_i			: in std_logic;							-- Internal FPGA.
		Axle_X_Vent_timer_trip_i			: in std_logic;							-- Internal FPGA.
		Axle_X_Health_timer_trip_i			: in std_logic;							-- Internal FPGA.

		Axle_X_Timer_trip_o					: out std_logic;						-- Internal FPGA.
		Axle_X_TIMER_TRIP_LW_o				: out std_logic							-- Internal FPGA.

				  
		  
    );
end entity axle_x_timer_trip;

architecture imp_axle_x_timer_trip of axle_x_timer_trip is

	-- local signals
	

begin

-- simple OR logic
Axle_X_Timer_trip_o <= Axle_X_Hold_timer_trip_i or Axle_X_Vent_timer_trip_i or Axle_X_Health_timer_trip_i;
Axle_X_TIMER_TRIP_LW_o <= Axle_X_Timer_trip_o;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_timer_trip;

