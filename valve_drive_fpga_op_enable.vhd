-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  valve_drive_fpga_op_enable.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the valve drive fpga output enable signal
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity valve_drive_fpga_op_enable is
    port (


	   	SB_AXLE1_HOLD_i						: in std_logic;							-- Command from micro.
		SB_AXLE1_VENT_i						: in std_logic;							-- Command from micro.
		SB_AXLE2_HOLD_i						: in std_logic;							-- Command from micro.
		SB_AXLE2_VENT_i						: in std_logic;							-- Command from micro.

		Axle_1_SB_Alarm_o					: out std_logic;							-- Internal FPGA.
		Axle_2_SB_Alarm_o					: out std_logic;							-- Internal FPGA.

	   WSP_AXLE1_HOLD_i					: in std_logic;							-- Command from micro.
		WSP_AXLE1_VENT_i					: in std_logic;							-- Command from micro.
		WSP_AXLE2_HOLD_i					: in std_logic;							-- Command from micro.
		WSP_AXLE2_VENT_i					: in std_logic;							-- Command from micro.

		Axle_1_WSP_Alarm_o					: out std_logic;						-- Internal FPGA.
		Axle_2_WSP_Alarm_o					: out std_logic;						-- Internal FPGA.
		
		EMERGENCY1_i						: in std_logic;							-- Signal from emergency digital input
		EMERGENCY2_i						: in std_logic;							-- Signal from emergency digital input
				
		Emergency_Signal_Alarm_o			: out std_logic;						-- internal fpga signal				
		
		VALVE_DRIVE_FPGA_OP_ENABLE_o		: out std_logic							-- Output to SAFETY INTERLOCK 24V Interlock circuit

		  
    );
end entity valve_drive_fpga_op_enable;

architecture imp_valve_drive_fpga_op_enable of valve_drive_fpga_op_enable is

-- local signals
signal	Axle_1_SB_Alarm_s			: std_logic;
signal	Axle_2_SB_Alarm_s			: std_logic;
	
signal	Axle_1_WSP_Alarm_s			: std_logic;
signal	Axle_2_WSP_Alarm_s			: std_logic;

signal 	Emergency_Signal_Alarm_s	: std_logic;

begin

-- logic

Axle_1_SB_Alarm_o <= Axle_1_SB_Alarm_s;
Axle_2_SB_Alarm_o <= Axle_2_SB_Alarm_s;

Axle_1_WSP_Alarm_o <= Axle_1_WSP_Alarm_s;
Axle_2_WSP_Alarm_o <= Axle_2_WSP_Alarm_s;

Emergency_Signal_Alarm_o <= Emergency_Signal_Alarm_s;


Axle_1_SB_Alarm_s <= SB_AXLE1_VENT_i and not SB_AXLE1_HOLD_i;
Axle_2_SB_Alarm_s <= SB_AXLE2_VENT_i and not SB_AXLE2_HOLD_i;

Axle_1_WSP_Alarm_s <= WSP_AXLE1_VENT_i and not WSP_AXLE1_HOLD_i;
Axle_2_WSP_Alarm_s <= WSP_AXLE2_VENT_i and not WSP_AXLE2_HOLD_i;

Emergency_Signal_Alarm_s <= EMERGENCY1_i xor EMERGENCY2_i;

VALVE_DRIVE_FPGA_OP_ENABLE_o <= not (Axle_1_SB_Alarm_s or Axle_2_SB_Alarm_s or Axle_1_WSP_Alarm_s or Axle_2_WSP_Alarm_s or Emergency_Signal_Alarm_s);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_valve_drive_fpga_op_enable;

