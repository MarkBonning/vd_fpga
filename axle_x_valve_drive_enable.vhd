-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  output_latch.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  16/10/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Block to set valve drive disable when axle x timer trip is active during emergency
-- and reset valve drive disable when the drive demand has been inactive for 50ms and
-- the timer trip is not active and not in emergency
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_valve_drive_enable is
    port (

		clock10khz_i								: in std_logic;						-- 10 kHz clock
		reset_ni										: in std_logic;						-- reset (active low)

		axle_x_valve_drive_demand_i			: in std_logic;
		
		axle_x_timer_trip_i						: in std_logic;
		
		emergency1_i								: in std_logic;
		
		emergency2_i								: in std_logic;
		
		axle_x_valve_drive_enable_o			: out std_logic
				  
		  
    );
end entity axle_x_valve_drive_enable;

architecture imp_axle_x_valve_drive_enable of axle_x_valve_drive_enable is

signal 	timer_run_s								: std_logic;							-- flag to state the timer needs to be running
signal 	timer_timeout_s						: std_logic;							-- flag to state the timer has finished

signal 	axle_x_valve_drive_disable_s		: std_logic;
	

begin

-- logic

-- define when the 50ms timer runs
timer_run_s <= (not (axle_x_timer_trip_i and (emergency1_i or emergency2_i))) and not axle_x_valve_drive_demand_i;

-- define the enable logic
axle_x_valve_drive_enable_o <= axle_x_valve_drive_demand_i and not axle_x_valve_drive_disable_s;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to flag a 50ms delay has occured 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_timer : process (reset_ni, clock10khz_i)

	variable count_50ms      	: integer range 0 to 500 := 0;
	constant count_50ms_limit 	: integer range 0 to 500 := 500;				-- 50ms (100us x 500)
	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then
			count_50ms := 0;
			timer_timeout_s <= '0';

		-- clock data in on rising edge of slow clock
		elsif rising_edge(clock10khz_i) then	 

			if timer_run_s = '0' then
				count_50ms := 0;
				timer_timeout_s <= '0';
				
			elsif timer_run_s = '1' and count_50ms < count_50ms_limit then
				count_50ms := count_50ms + 1;
				timer_timeout_s <= '0';
				
			else
				timer_timeout_s <= '1';
			end if;

	 	end if;
	end process;	


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Process to control the valve drive disable logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_vd_disable : process (reset_ni, clock10khz_i)

	
	begin
		-- clear registers when reset is low
		if(reset_ni = '0') then

			axle_x_valve_drive_disable_s	<= '0';

		-- 
		elsif rising_edge(clock10khz_i) then	 
		
			if axle_x_timer_trip_i = '1' and (emergency1_i = '1' or emergency2_i = '1') then

				axle_x_valve_drive_disable_s	<= '1';
				
			elsif timer_timeout_s = '1' then

				axle_x_valve_drive_disable_s	<= '0';

			end if;

	 	end if;
	end process;	



-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_valve_drive_enable;

