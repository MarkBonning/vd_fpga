-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD FPGA
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  vd_constants.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning
--                     Date  :  16/10/2019
--
-- -------------------------------------------------------------------------------
-- description
-- Constants for Valve Drive fpga
-- -------------------------------------------------------------------------------
-- revision 00
--
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;



entity vd_constants is
    port (

		nc_target_current_A_o			: out std_logic_vector(7 downto 0);
		no_target_current_A_o			: out std_logic_vector(7 downto 0)
		
				  
		  
    );
end entity vd_constants;

architecture imp_vd_constants of vd_constants is

constant NC_TARGET_CURRENT_A        		: std_logic_vector( 7 downto 0) := x"20";
constant NO_TARGET_CURRENT_A        		: std_logic_vector( 7 downto 0) := x"30";
	

begin

-- logic

	nc_target_current_A_o	<=	NC_TARGET_CURRENT_A;
	no_target_current_A_o	<= 	NO_TARGET_CURRENT_A;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_vd_constants;





























































