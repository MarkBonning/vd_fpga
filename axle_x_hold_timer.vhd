-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_hold_timer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the axle X hold timer of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_hold_timer is
    port (
    	
    	clock20mhz_i						: in std_logic;							-- system clock
    	reset_ni								: in std_logic;							-- system reset	

		axle_x_timer_enable_i			: in std_logic;							-- Internal FPGA signal to Inlet1 and Vent1 timer blocks
		axle_x_hold_mv_I_i					: in std_logic_vector(7 downto 0);		-- Axle X Hold MV A/D measured current.
		hold_drive_running_i				: in std_logic;							-- Output signal from FPGA to Axle X Hold magnet valve drive to state PWM > 0%


		axle_x_hold_timer_trip_o		: out std_logic;						-- Internal FPGA.

		hold_timeout_period_i			: in std_logic_vector(7 downto 0);		-- Microcontroller Application configuration.
		Hold_Off_Period_i					: in std_logic_vector(7 downto 0);		-- Microcontroller Application configuration.
		
		pulse_1ms_i							: std_logic;									-- single clock pulse (20MHz) every 1ms
		
		hold_timer_count_o				: out std_logic_vector(15 downto 0);
		
		hold_timer_running_o				: out std_logic
		
				  
		  
    );
end entity axle_x_hold_timer;

architecture imp_axle_x_hold_timer of axle_x_hold_timer is

	-- local signals

	
	signal	hold_timer_count_s				: unsigned(15 downto 0);					-- 1ms resolution counter for the hold timer timeout and hold off periods
	
	constant vent_current_10mA_s			: std_logic_vector(7 downto 0) := x"0a";	-- 10mA - assumeing LSB = 1mA
	
	signal hold_timeout_period_s			: unsigned(15 downto 0);					
	signal hold_off_period_s				: unsigned(15 downto 0);					
	signal hold_timer_total_period_s		: unsigned(15 downto 0);					
																					
	type states is ( HOLD_TIMER_RESET, HOLD_TIMER_RUN, HOLD_TIMER_HOLD_OFF );
	signal state_s   	: states;
																					
	

begin

-- logic
	hold_timer_count_o <= std_logic_vector(hold_timer_count_s);


	hold_timer_running_o <= '1' when state_s = HOLD_TIMER_RUN else '0';

	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- create axle x hold trip output
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	hold_trip : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			hold_timer_count_s 			<= (others => '0');
			hold_timeout_period_s 		<= (others => '0');
			hold_off_period_s 			<= (others => '0');
			hold_timer_total_period_s	<= (others => '0');

			axle_x_hold_timer_trip_o <= '0';
			
			state_s <= HOLD_TIMER_RESET;
			
		
			
		elsif rising_edge (clock20mhz_i) then
		
			-- the timeout period signal comes from the timeout period register.
			-- the minimum period is 4 seconds
			-- the register value has a resolution of 50ms and is increments of 50ms on top of the minimum 4 seconds
			-- the max timeout is 15 seconds, so any value in the register greater than 140 is too big, so set to the max 15 seconds.
			if unsigned(hold_timeout_period_i) > 140 then
				hold_timeout_period_s <= to_unsigned(15000,hold_timeout_period_s'length);
			else 
				hold_timeout_period_s <= 8000 + (unsigned(hold_timeout_period_i) * 50);
			end if;


			-- the hold off period signal comes from the hold off period register. If less  greater than 15 seconds, then set to 15 seconds
			if unsigned(hold_off_period_i) > 140 then
				hold_off_period_s <= to_unsigned(15000, hold_off_period_s'length);
			else 
				hold_off_period_s <= 8000 + (unsigned(hold_off_period_i) * 50);
			end if;

			-- total period for the timer = timeout period + hold off period
			hold_timer_total_period_s <= hold_timeout_period_s + hold_off_period_s;

			-- state machine for the timer trip function		
			case state_s is
				when HOLD_TIMER_RESET =>

					-- trip is reset
					axle_x_hold_timer_trip_o <= '0';
					
					-- clear timer count
					hold_timer_count_s 		<= (others => '0');
					
					

					-- timer enable = 1 and (hold current > 10mA or hold_drive_running_i PWM is running)
					if axle_x_timer_enable_i = '1' and (axle_x_hold_mv_I_i > vent_current_10mA_s or hold_drive_running_i = '1') then
						state_s <= HOLD_TIMER_RUN;
					else
						state_s <= HOLD_TIMER_RESET;
					end if;

				when HOLD_TIMER_RUN =>

					-- trip remains at reset
					axle_x_hold_timer_trip_o <= '0';
					
			

					-- if the timeout period has been met, move to the hold off state
					if hold_timer_count_s = hold_timeout_period_s then
						state_s <= HOLD_TIMER_HOLD_OFF;

					-- if hold current < 10mA and hold_drive_running_i PWM is stopped, return to reset state
					elsif axle_x_hold_mv_I_i < vent_current_10mA_s and hold_drive_running_i = '0' then
						state_s <= HOLD_TIMER_RESET;

					-- if the timer enable is reset, return to reset state
					elsif axle_x_timer_enable_i = '0' then
						state_s <= HOLD_TIMER_RESET;

					-- on each 1ms pulse increment counter
					elsif pulse_1ms_i = '1' then
						hold_timer_count_s <= hold_timer_count_s + 1;
					
					-- else stay in current state						
					else
						state_s <= HOLD_TIMER_RUN;
					end if;

				when HOLD_TIMER_HOLD_OFF =>

					-- trip is set
					axle_x_hold_timer_trip_o <= '1';

					
					-- if the total period (timeout + hold off) has been met, return to reset state
					if hold_timer_count_s = hold_timer_total_period_s then
						state_s <= HOLD_TIMER_RESET;
					
					-- else if 1ms pulse, increment counter
					elsif pulse_1ms_i = '1' then
						hold_timer_count_s <= hold_timer_count_s + 1;
						state_s <= HOLD_TIMER_HOLD_OFF;

					-- else remain in current state
					else
						state_s <= HOLD_TIMER_HOLD_OFF;
					end if;

				-- something went wrong, reset state machine
				when others =>
					state_s <= HOLD_TIMER_RESET;
			end case;
				

		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_hold_timer;

