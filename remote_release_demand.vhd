-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  remote_release_demand.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  15/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Generates the remote_release_demand signal
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity remote_release_demand is
    port (

		remote_release_filtered_i			: in std_logic;							-- Internal FPGA.
		brake_pipe_control_i				: in std_logic;							-- Internal FPGA.

		remote_release_demand_o				: out std_logic							-- Internal FPGA.
				  
		  
    );
end entity remote_release_demand;

architecture imp_remote_release_demand of remote_release_demand is

	-- local signals

begin

-- logic
remote_release_demand_o <= not remote_release_filtered_i and not brake_pipe_control_i;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_remote_release_demand;

