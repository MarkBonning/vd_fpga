-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  SB_WSP_valve_drive_demand.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  02/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the SB and WSP valve demand signals of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity SB_WSP_valve_drive_demand is
    port (
    	
		axle_x_valve_drive_enable_i			: in std_logic;
		
		sb_axle_x_hold_i						: in std_logic;							-- Command from Microprocessor
		sb_axle_x_vent_i						: in std_logic;							-- Command from Microprocessor
		wsp_axle_x_hold_i					: in std_logic;							-- Command from Microprocessor
		wsp_axle_x_vent_i					: in std_logic;							-- Command from Microprocessor
					
		emergency_1_i						: in std_logic;							-- Command from Microprocessor
		emergency_2_i						: in std_logic;							-- Command from Microprocessor

		internal_lbsr_vd_fpga_i				: in std_logic;							-- low BSR transducer
		external_lbsr_trip_vd_fpga_i			: in std_logic;							-- low BSR transducer

		axle_x_timer_enable_i				: in std_logic;							-- Internal FPGA signal

		axle_x_valve_drive_demand_o		: out std_logic;						-- internal fpga signal to output latch module

		axle_x_hold_demand_o				: out std_logic;						-- Internal FPGA signal
		axle_x_vent_demand_o				: out std_logic;						-- Internal FPGA signal

		external_lbsr_enabled_i				: in std_logic;							-- Microcontroller Application configuration. 
		internal_lbsr_enabled_i				: in std_logic							-- Microcontroller Application configuration. 
		
		
    );
end entity SB_WSP_valve_drive_demand;

architecture imp_SB_WSP_valve_drive_demand of SB_WSP_valve_drive_demand is

	-- local signals
	signal	emergency_s							: std_logic;
	signal 	low_bsr_s						: std_logic;							-- Low BSR signal																					
	

begin

-- logic

-- Emergency condition is either emergency signal set
emergency_s <= emergency_1_i or emergency_2_i;							

low_bsr_s	<= (external_lbsr_enabled_i and external_lbsr_trip_vd_fpga_i) or (internal_lbsr_enabled_i and internal_lbsr_vd_fpga_i);

axle_x_hold_demand_o	<= axle_x_valve_drive_enable_i;

axle_x_vent_demand_o	<= axle_x_valve_drive_enable_i and ((sb_axle_x_vent_i and (low_bsr_s or not axle_x_timer_enable_i)) or ((wsp_axle_x_vent_i or (sb_axle_x_vent_i and not emergency_s)) 
							and not low_bsr_s and axle_x_timer_enable_i));

axle_x_valve_drive_demand_o <= (sb_axle_x_hold_i and (low_bsr_s or not axle_x_timer_enable_i)) or (wsp_axle_x_hold_i and not low_bsr_s and axle_x_timer_enable_i);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_SB_WSP_valve_drive_demand;

