-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  s25fl128s_flash.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  25/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls an SPI master to read the status register of the s25fl128s flash device
-- for test FPGAs only
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
USE ieee.std_logic_arith.all;


entity s25fl128s_flash is
    port 	(

      clock20mhz_i           :  in std_logic;
      reset_ni               :  in std_logic;
        
      enable_o				: out std_logic;
      cpol_o				: out std_logic;
		cpha_o				: out std_logic;
		cont_o				: out std_logic;
		addr_o				: out std_logic_vector(7 downto 0);
		tx_data_o			: out std_logic_vector(23 downto 0);
		busy_i				: in std_logic;
		rx_data_i			: in std_logic_vector(23 downto 0);
		rx_data_o			: out std_logic_vector(15 downto 0)
		

        );
end;

architecture RTL of s25fl128s_flash is


type states is ( DELAY, START, RUN, BUSY, DONE);
signal state_machine_s   	: states;

	
begin

	-- drive constant pins
	cpol_o <= '0';
	cpha_o <= '0';
	addr_o <= (others => '0');
	cont_o <= '0';
	tx_data_o <= x"050000";

	-- map signals to ports
	


    --counter to set the watchdog refresh signal
    s25fl128s_flash_read : 	process ( reset_ni, clock20mhz_i ) is

	variable count : integer range 0 to 255:= 0;
    
    
    begin
        if ( reset_ni = '0' ) then
            enable_o <= '0';
            count := 0;
				rx_data_o <= (others => '0');
            state_machine_s <= DELAY;

        elsif Rising_Edge ( clock20mhz_i ) then
        

					-- state machine to run the transfer to the SPI module
					case state_machine_s is
		        		when DELAY =>
							if( count >= 200) then
								state_machine_s <= START;
							else
								count := count + 1;
								state_machine_s <= DELAY;
							end if;
						when START =>
							enable_o <= '1';
							state_machine_s <= RUN;
						when RUN =>
							enable_o <= '0';
							if busy_i = '0' then
								state_machine_s <= RUN;
							else
								state_machine_s <= BUSY;
							end if;
						when BUSY =>
							if busy_i = '0' then
								state_machine_s <= DONE;
							else
								state_machine_s <= BUSY;
							end if;
						when DONE =>
							rx_data_o <= rx_data_i(15 downto 0);
		        			count := 0;
							state_machine_s <= DELAY;

		        		when others =>
		        			count := 0;
							state_machine_s <= DELAY;
					end case;		          
					
        end if;
    end process;






end architecture RTL;