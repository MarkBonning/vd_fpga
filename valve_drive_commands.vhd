-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  valve_drive_commands.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  30/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the valve_drive_commands of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity valve_drive_commands is
    port (
    	
    	clock20mhz_i						: in std_logic;							-- system clock
    	reset_ni							: in std_logic;							-- system reset	

		demand_i							: in std_logic;							-- magnet valve drive command, internal FPGA signal.

		valve_drive_o						: out std_logic;						-- Output signal from FPGA to magnet valve drive FET.

		mv_current_A_i						: in std_logic_vector(7 downto 0);		-- Magnetic valve current.
		
		pickup_time_i						: in std_logic_vector(7 downto 0);		-- number of milliseconds for time pickup time "B"

		target_current_i					: in std_logic_vector(7 downto 0);		-- target back off MV current "A"
		
		pulse_1ms_i							: in std_logic;								-- single clock pulse (20MHz) every 1ms
		
		valve_drive_running_o			: out std_logic								-- flag to inform the hold and vent timers that the valve drive PWM is > 0%
		
		
    );
end entity valve_drive_commands;

architecture imp_valve_drive_commands of valve_drive_commands is

-- local signals
	signal	Pickup_count_s				: unsigned(7 downto 0);					-- hold 1ms counts, 8 bits give 0 to 255ms range, 1ms resolution

	signal	count_PWM_s					: unsigned(9 downto 0);						-- counter to hold current clock count to create 20kHz PWM period
	signal	high_count_PWM_s			: unsigned(9 downto 0);						-- counter to hold clock count for the high level PWM period
	constant count_PWM_limit_s			: unsigned(9 downto 0) := "1111101000"; 	-- number of clocks to reach 50us (20kHz)

	signal Target_current_s				: std_logic_vector(7 downto 0);
	
	type states is ( MV_De_energised, MV_Pick_up, MV_Back_off );
	signal state_s   	: states;


begin

-- logic

	-- if register target current is 0 then assume 32mA
	Target_current_s <= x"20" when target_current_i = x"00" else target_current_i;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MV PWM generator
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	MV_PWM : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			-- clear PWM count
			count_PWM_s 		<= (others => '0');
			
		elsif rising_edge (clock20mhz_i) then

			-- check if hit the 50us limit
			if count_PWM_s = count_PWM_limit_s - 1 then
				count_PWM_s <= (others => '0');
				
			-- else increment the count
			else
				count_PWM_s <= count_PWM_s + 1;
			end if;
			
		end if;
	end process;
	
	
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- control digital outputs
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	proc_valve_drive_commands : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then

			-- clear the valve drive
			valve_drive_o <= '0';
		
			-- clear pick up counter 
			Pickup_count_s <= (others => '0');
		
			-- assume PWM high count starts at 100% as the Pckup state require 100% on prior to Back off
			high_count_PWM_s	<= count_PWM_limit_s;
			
			
			-- PWM not running
			valve_drive_running_o	<= '0';
		
		
			-- initial stae
			state_s <= MV_De_energised;
		
		elsif rising_edge (clock20mhz_i) then

			case state_s is
			
				-- initial state is MV value off
				when MV_De_energised =>

					-- clear the valve drive
					valve_drive_o <= '0';
					
					-- PWM not running
					valve_drive_running_o	<= '0';
					
				
					-- clear pick up counter 
					Pickup_count_s <= (others => '0');


					-- if demand requested then turn on MV
					if demand_i = '1' then
						state_s <= MV_Pick_up;

					-- else stay in current state						
					else
						state_s <= MV_De_energised;
					
					end if;
				
				-- pickup state is MV value signal fully on
				when MV_Pick_up =>
				

					-- set the valve drive (100%)
					valve_drive_o <= '1';
					
					-- PWM running
					valve_drive_running_o	<= '1';
					

					-- assume PWM high count starts at 100% as the Pickup state requires 100% on prior to Back off
					high_count_PWM_s	<= count_PWM_limit_s;

					-- on each 1ms pulse increment counter
					if pulse_1ms_i = '1' then
						Pickup_count_s <= Pickup_count_s + 1;
					end if;

					-- if no demand, return to start
					if demand_i = '0' then
						state_s <= MV_De_energised;
						
					--check for t='B'ms
					elsif Pickup_count_s = unsigned(pickup_time_i) then
						state_s <= MV_Back_off;

					-- else stay in current state
					else
						state_s <= MV_Pick_up;
					end if;

				-- backoff state is MV value signal PWM'd to match target current
				when MV_Back_off =>
				
		
					-- tell the timers if running or not based on PWM > 0%
					if high_count_PWM_s > 0 then
						-- PWM running
						valve_drive_running_o	<= '1';
					else
						-- PWM not running
						valve_drive_running_o	<= '0';
					end if;
					
		
					-- use the PWM period as the timer for PWM adjustment. Otherwise at 20MHz the PWM count will 
					-- increment and decrement faster than the ADC will measure the response so will oscillate.
					-- this also only changes the PWM value on the boundary to stop unwanted mid cycle pulses
					if count_PWM_s = 0 then
			
						-- if the MV current is too high, reduce the PWM high period
						if mv_current_A_i > Target_current_s then

							-- only adjust if greater than 0
							if high_count_PWM_s > 0 then
								high_count_PWM_s <= high_count_PWM_s - 1;
							end if;

						-- if the MV current is too low, increase the PWM high period
						elsif mv_current_A_i < Target_current_s then

							-- only adjust if less than limit
							if high_count_PWM_s < count_PWM_limit_s then
								high_count_PWM_s <= high_count_PWM_s + 1;
							end if;

						end if;
					
					-- else keep the count the same
					else
						high_count_PWM_s <= high_count_PWM_s;
					end if;
					
					-- create PWM output
					-- high period, MV on
					if count_PWM_s < high_count_PWM_s then
						valve_drive_o <= '1';
						
					-- low period, MV off
					else
						valve_drive_o <= '0';
					end if;

					
					-- if no demand, return to start
					if demand_i = '0' then
						state_s <= MV_De_energised;

					-- else stay in current state
					else
						state_s <= MV_Back_off;
					end if;

				-- something went wrong, return to start state
				when others =>
				
					state_s <= MV_De_energised;
				
			end case;

		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_valve_drive_commands;

