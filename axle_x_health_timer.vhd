-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  axle_x_health_timer.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  30/04/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the axle X health timer of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity axle_x_health_timer is
    port (
    	
    	clock20mhz_i						: in std_logic;							-- system clock
    	reset_ni								: in std_logic;							-- system reset	

		wsp_axle_x_healthy_i				: in std_logic;							-- Command from Microprocessor

		axle_x_health_timer_trip_o		: out std_logic;						-- Internal FPGA.

		vent_off_period_i					: in std_logic_vector(7 downto 0);		-- Microcontroller Application configuration.
		
		pulse_1ms_i							: in std_logic;								-- single clock pulse (20MHz) every 1ms
		
		healthy_timer_count_o			: out std_logic_vector(15 downto 0)	-- timer back to Microprocessor
				  
		  
    );
end entity axle_x_health_timer;

architecture imp_axle_x_health_timer of axle_x_health_timer is

	
	signal	healthy_timer_count_s				: unsigned(15 downto 0);					-- 10ms resolution counter for the hold timer timeout and hold off periods
	
	signal vent_off_period_s				: unsigned(15 downto 0);					
																					
	type states is ( HEALTHY_TIMER_RESET, UNHEALTHY_TIMER_RESET, HEALTHY_TIMER_RUN );
	signal state_s   	: states;
																					
	

begin

-- logic
	healthy_timer_count_o <= std_logic_vector(healthy_timer_count_s);

	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- create axle x vent trip output
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	healthy_trip : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			healthy_timer_count_s 		<= (others => '0');
			vent_off_period_s 	<= (others => '0');

			axle_x_health_timer_trip_o <= '0';
			
			state_s <= HEALTHY_TIMER_RESET;
			
		
			
		elsif rising_edge (clock20mhz_i) then
		
			-- the timeout period signal comes from the timeout period register.
			-- the minimum period is 4 seconds
			-- the register value has a resolution of 50ms and is increments of 50ms on top of the minimum 4 seconds
			-- the max timeout is 10 seconds, so any value in the register greater than 120 is too big, so set to the max 10 seconds.

			if unsigned(vent_off_period_i) > 120 then
				vent_off_period_s <= to_unsigned(10000, vent_off_period_s'length);
			else 
				vent_off_period_s <= 4000 + (unsigned(vent_off_period_i) * 50);
			end if;


			-- state machine for the timer trip function		
			case state_s is
				when HEALTHY_TIMER_RESET =>

					-- trip is reset
					axle_x_health_timer_trip_o <= '0';
					
					-- clear timer count
					healthy_timer_count_s <= (others => '0');
					
					-- WSP X HEALTHY = 0, move to unhealthy state
					if wsp_axle_x_healthy_i = '0' then
						state_s <= UNHEALTHY_TIMER_RESET;
					else
						state_s <= HEALTHY_TIMER_RESET;
					end if;

				when UNHEALTHY_TIMER_RESET =>

					-- trip gets set
					axle_x_health_timer_trip_o <= '1';
					
					-- clear timer count
					healthy_timer_count_s <= (others => '0');
					

					-- WSP X HEALTHY = 1, move to healthy run state
					if wsp_axle_x_healthy_i = '1' then
						state_s <= HEALTHY_TIMER_RUN;
					-- else remain in current state
					else
						state_s <= UNHEALTHY_TIMER_RESET;
					end if;
				

				when HEALTHY_TIMER_RUN =>

					-- trip is set
					axle_x_health_timer_trip_o <= '1';

			
					-- on each 1ms pulse increment counter
					if pulse_1ms_i = '1' then
						healthy_timer_count_s <= healthy_timer_count_s + 1;
					end if;
					
					-- WSP X HEALTHY = 0, move to unhealthy state
					if wsp_axle_x_healthy_i = '0' then
						state_s <= UNHEALTHY_TIMER_RESET;
					-- if the vent off period has been met, return to reset state
					elsif healthy_timer_count_s = vent_off_period_s then
						state_s <= HEALTHY_TIMER_RESET;
					
					-- else remain in current state
					else
						state_s <= HEALTHY_TIMER_RUN;
					end if;

				-- something went wrong, set state to reset
				when others =>
						state_s <= HEALTHY_TIMER_RESET;
			end case;
				

		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_axle_x_health_timer;

