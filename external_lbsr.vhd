-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  external_lbsr.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Controls the external lbsr of the Valve Drive FPGA
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity external_lbsr is
    port (
    	
    	clock20mhz_i						: in std_logic;							-- system clock
    	reset_ni							: in std_logic;							-- system reset	

		EXTERNAL_LBSR_TRIP_VD_FPGA_i		: in std_logic;							-- Input from external LBSR input circuit
		
		EXT_LBSR_PULSE_ENABLE_o				: out std_logic;						-- Output to control external lbsr pulse circuit.
		ext_lbsr_Fit_o						: out std_logic						-- Internal FPGA signal.
		
		
    );
end entity external_lbsr;

architecture imp_external_lbsr of external_lbsr is

-- local signals

signal	count_1ms_s					: unsigned(15 downto 0);					-- counter to hold current clock count to create 10ms pulse
signal	count_1ms_pulse_s			: std_logic;								-- signal clock pulse on this signal when 1ms count limit is reached
constant count_1ms_limit_s			: unsigned(15 downto 0) := "0100111000100000"; -- number of clocks to reach 1ms

signal count_30ms_s					: unsigned(4 downto 0);						-- counter for the 30ms delay
constant count_30ms_limit_s			: unsigned(4 downto 0) := "11110";			-- number of clocks to reach 30ms
signal	count_30ms_enabled_s		: std_logic;								-- flag to indicate the counter is running

signal count_500ms_s				: unsigned(8 downto 0);						-- counter for the 500ms delay
constant count_500ms_limit_s		: unsigned(8 downto 0) := "111110100";		-- number of clocks to reach 500ms
signal	count_500ms_enabled_s		: std_logic;								-- flag to indicate the counter is running

signal count_62ms_s					: unsigned(5 downto 0);						-- counter for the 62ms delay
constant count_62ms_limit_s			: unsigned(5 downto 0) := "111110";			-- number of clocks to reach 62ms
signal	count_62ms_enabled_s		: std_logic;								-- flag to indicate the counter is running

signal pulse_timer_s				: std_logic;								-- used for creting REM_REL_PULSE_ENABLE state

signal s0_s							: std_logic;								-- edge detect signal 0
signal s1_s							: std_logic;								-- edge detect signal 1

signal rising_detect_s				: std_logic;								-- single pulse on rising edge
signal falling_detect_s				: std_logic;								-- single pulse on rising edge

signal ext_lbsr_Fit_s				: std_logic;								-- signal version of ext_lbsr_Fit_o

begin

-- logic

-- single clock pulse on rising edge of EXTERNAL_LBSR_TRIP_VD_FPGA_i
rising_detect_s <= not s1_s and s0_s;

-- single clock pulse on falling edge of EXTERNAL_LBSR_TRIP_VD_FPGA_i
falling_detect_s <= not s0_s and s1_s;

--- pulse timer is high when either 500ms count is enabled or ext_lbsr_Fit_s is high
pulse_timer_s <= count_500ms_enabled_s or ext_lbsr_Fit_s;

--- EXT_LBSR_PULSE_ENABLE_o is high when either the pulse time is low or the 62ms counter is enabled
EXT_LBSR_PULSE_ENABLE_o <= not pulse_timer_s or count_62ms_enabled_s;

ext_lbsr_Fit_o <= ext_lbsr_Fit_s;

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- create a single clock pulse every 1ms
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	count_1ms : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			count_1ms_s 		<= (others => '0');
			count_1ms_pulse_s 	<= '0';
		elsif rising_edge (clock20mhz_i) then
			if count_1ms_s = count_1ms_limit_s then
				count_1ms_pulse_s 	<= '1';
				count_1ms_s <= (others => '0');
			-- running so increment the count
			else
				count_1ms_s <= count_1ms_s + 1;
				count_1ms_pulse_s 	<= '0';
			end if;
		end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Rising and falling edge logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rising_falling : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			s0_s <= '0';
			s1_s <= '0';
		elsif rising_edge (clock20mhz_i) then
			s0_s <= EXTERNAL_LBSR_TRIP_VD_FPGA_i;
			s1_s <= s0_s;
		end if;
	end process;

	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- control remote release fit
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	remote_release_fit : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			
			count_30ms_s 	<= (others => '0');
			count_30ms_enabled_s <= '0';
			ext_lbsr_Fit_s <= '0';
		
		elsif rising_edge (clock20mhz_i) then

			-- if the 30ms counter ends, then set ext_lbsr_Fit_o to EXTERNAL_LBSR_TRIP_VD_FPGA_i, clear and disable count
			if count_30ms_s = count_30ms_limit_s then
				count_30ms_enabled_s <= '0';
				count_30ms_s 	<= (others => '0');
				ext_lbsr_Fit_s <= EXTERNAL_LBSR_TRIP_VD_FPGA_i;

			-- else on rising edge or falling edage of EXTERNAL_LBSR_TRIP_VD_FPGA_i, clear and enable the counter
			elsif rising_detect_s = '1' or falling_detect_s = '1' then
				count_30ms_enabled_s <= '1';
				count_30ms_s 	<= (others => '0');
			else
				-- if 30ms counter is enabled, then on every 1ms pulse, increment the count
				if count_30ms_enabled_s = '1' and count_1ms_pulse_s = '1' then
					count_30ms_s <= count_30ms_s + 1;
				end if;

			end if;
		end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- control pulse timer
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	pulse_timer : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			
			count_500ms_s 	<= (others => '0');
			count_500ms_enabled_s <= '0';
		
		elsif rising_edge (clock20mhz_i) then
	
			-- if the 500ms period has been met, stop and clear the count 
			if count_500ms_s = count_500ms_limit_s then
				count_500ms_enabled_s <= '0';
				count_500ms_s <= (others => '0');

			-- else on rising edage of EXTERNAL_LBSR_TRIP_VD_FPGA_i, with ext_lbsr_Fit_s low start 500ms counter
			elsif rising_detect_s = '1' and ext_lbsr_Fit_s = '0' then
				count_500ms_enabled_s <= '1';

			else
				-- if 500ms counter is enabled, then on every 1ms pulse, increment the count
				if count_500ms_enabled_s = '1' and count_1ms_pulse_s = '1' then
					count_500ms_s <= count_500ms_s + 1;
				end if;
			
			end if;
		end if;
	end process;
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- control REM_REL_PULSE_ENABLE
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
	rem_rel_pulse_enable : process (reset_ni, clock20mhz_i)
	begin
		if reset_ni = '0' then
			
			count_62ms_s 	<= (others => '0');
			count_62ms_enabled_s <= '0';

		elsif rising_edge (clock20mhz_i) then

			-- if the 62ms period has been met, stop and clear the count 
			if count_62ms_s = count_62ms_limit_s then
				count_62ms_enabled_s <= '0';
				count_62ms_s <= (others => '0');

			-- else on rising edage of EXTERNAL_LBSR_TRIP_VD_FPGA_i and pulse timer low, start 62ms counter
			elsif rising_detect_s = '1' and ext_lbsr_Fit_s = '0' then
				count_62ms_enabled_s <= '1';

			else

				-- if 62ms counter is enabled, then on every 1ms pulse, increment the count
				if count_62ms_enabled_s = '1' and count_1ms_pulse_s = '1' then
					count_62ms_s <= count_62ms_s + 1;
				end if;
			
			end if;
		end if;
	end process;


-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_external_lbsr;

