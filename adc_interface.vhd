-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  NG PVU
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_interface.vhd
--                   Author  :  Mark Bonning 
--                     Date  :  17/10/2018 
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Interface to the internal adc hardcore
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning - based on design by Leesa Kingman. 
--             Changes to channel control       
-- -------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use work.pvuConstants.all;

entity adc_interface is
	port 	(
		clock20mhz_i     	:  in std_logic; 
		reset_ni  			    	:  in std_logic;

		reset_sink_reset_n  	: out std_logic;
		response_valid_i      	:  in std_logic;
		response_channel_i    	:  in std_logic_vector( 4 downto 0);
		response_data_i       	:  in std_logic_vector(11 downto 0);
		sequencer_address_o   	: out std_logic;
		sequencer_write_o     	: out std_logic;
		sequencer_writedata_o 	: out std_logic_vector(31 downto 0);

		ch0_o						: out std_logic_vector(15 downto 0);
		ch1_o						: out std_logic_vector(15 downto 0);
		ch2_o						: out std_logic_vector(15 downto 0);
		ch3_o						: out std_logic_vector(15 downto 0);
		ch4_o						: out std_logic_vector(15 downto 0);
		ch5_o						: out std_logic_vector(15 downto 0);
		ch6_o						: out std_logic_vector(15 downto 0);
		ch7_o						: out std_logic_vector(15 downto 0);
		ch8_o						: out std_logic_vector(15 downto 0);
		temperature_o			: out std_logic_vector(15 downto 0)

		
		);
end;

architecture RTL of adc_interface is
	signal adc_register_channel_s : std_logic_vector( 4 downto 0);
	signal counter_s              : unsigned( 3 downto 0);
	signal adc_register_s         : unsigned(15 downto 0);

	signal ch0_s			    : unsigned(15 downto 0);
	signal ch1_s			    : unsigned(15 downto 0);
	signal ch2_s			    : unsigned(15 downto 0);
	signal ch3_s			   	: unsigned(15 downto 0);
	signal ch4_s			    : unsigned(15 downto 0);
	signal ch5_s			    : unsigned(15 downto 0);
	signal ch6_s			   	: unsigned(15 downto 0);
	signal ch7_s		        : unsigned(15 downto 0);
	signal ch8_s			   	: unsigned(15 downto 0);
	signal temperature_s	   	: unsigned(15 downto 0);
	
	signal data_write_s			: std_logic;
	
	
	type state_cm_wr is ( ready, adc_start, adc_run );

	signal set_sync_sm     :	state_cm_wr;


begin

	-- assign values
	reset_sink_reset_n <= '1';
	sequencer_address_o  <= '0';
	

	-- receive data from adc
	receive_data :process (reset_ni, clock20mhz_i) is
	begin
		if reset_ni = '0' then
			adc_register_channel_s <=  ( others => '0' ) ;
			adc_register_s <= ( others => '0' ) ;
			counter_s <=  ( others => '0' );
			data_write_s <= '0';
			
		elsif rising_edge ( clock20mhz_i ) then
			if counter_s = "1010" then
				counter_s <= "0000";
				data_write_s <= '0';
			elsif response_valid_i = '1' then -- valid for one clock cycle
				counter_s <=  counter_s + 1;
				adc_register_channel_s <= response_channel_i;
				adc_register_s <=  "0000" & unsigned(response_data_i);
				data_write_s <= '1';
			else
				data_write_s <= '0';
			end if;
		end if;
	end process;



	-- latch data to be correct register depending on the 4-bit address written
	address_decode_data : process (clock20mhz_i, reset_ni) is
	begin
		if reset_ni = '0' then
			ch0_s	<= (others => '0');
			ch1_s	<= (others => '0');
			ch2_s	<= (others => '0');
			ch3_s 	<= (others => '0');
			ch4_s	<= (others => '0');
			ch5_s	<= (others => '0');
			ch6_s 	<= (others => '0');
			ch7_s	<= (others => '0');
			ch8_s 	<= (others => '0');
			temperature_s 	<= (others => '0');

			

		elsif rising_edge ( clock20mhz_i ) then
		
			if data_write_s = '1' then
	
				if adc_register_channel_s = "00001" then	   --1
					ch0_s <= adc_register_s;
				elsif adc_register_channel_s = "00010" then	   --1
					ch1_s <= adc_register_s;
				elsif adc_register_channel_s = "00011" then --2
					ch2_s <= adc_register_s;
				elsif adc_register_channel_s = "00100" then --3
					ch3_s <= adc_register_s;
				elsif adc_register_channel_s = "00101" then --4
					ch4_s <= adc_register_s;
				elsif adc_register_channel_s = "00110" then --5
					ch5_s <= adc_register_s;
				elsif adc_register_channel_s = "00111" then --6
					ch6_s <= adc_register_s;
				elsif adc_register_channel_s = "01000" then --7
					ch7_s <= adc_register_s;
				elsif adc_register_channel_s = "01001" then --8
					ch8_s <= adc_register_s;
				elsif adc_register_channel_s = "00000" then --8
					temperature_s <= adc_register_s;
				end if;

			end if;
			
				
		end if;
	end process;

	ch0_o				<= std_logic_vector(ch0_s);
	ch1_o				<= std_logic_vector(ch1_s);
	ch2_o				<= std_logic_vector(ch2_s);
	ch3_o 			<= std_logic_vector(ch3_s);
	ch4_o				<= std_logic_vector(ch4_s);
	ch5_o				<= std_logic_vector(ch5_s);
	ch6_o 			<= std_logic_vector(ch6_s);
	ch7_o				<= std_logic_vector(ch7_s);
	ch8_o 			<= std_logic_vector(ch8_s);
	temperature_o 	<= std_logic_vector(temperature_s);

	

	
-- sets the adc start signal. runs once only
	process (reset_ni, clock20mhz_i) is
		variable count : integer range 0 to 10:= 0;
	begin
		if reset_ni = '0' then
			set_sync_sm <= READY;
			sequencer_write_o		<= '0';
			sequencer_writedata_o 	<=  (others => '0') ;
		elsif Rising_Edge ( clock20mhz_i ) then
			case set_sync_sm is
				when READY =>
					count := count + 1;
					if count = 9 then
						set_sync_sm      		<= adc_start;
					else
						set_sync_sm      		<= READY;
					end if;
				when adc_start =>
					sequencer_write_o 			<= '1';
					sequencer_writedata_o(0) 	<= '1';
					set_sync_sm      			<= adc_run;
				when adc_run =>
					sequencer_write_o 			<= '0';
					sequencer_writedata_o 		<= (others => '0') ;
					set_sync_sm  				<= adc_run;
			end case;
		end if;
	end process;


end architecture RTL;