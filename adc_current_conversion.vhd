-- -------------------------------------------------------------------------------
--
--       Copyright  (c)  2016  ;  Knorr-Bremse Rail Systems (UK) Ltd
--
--        This program is protected by copyright and the information
--         contained therein is confidential.  The program may not be
--          copied and the information may not be used or disclosed
--          except with the written permission of the proprietors
--                        Knorr-Bremse Rail Systems (UK) Ltd.
--
-- -------------------------------------------------------------------------------
--
--             Project Name  :  VD fpga
--                FPGA Name  :  Altera 10M08SAU169I7G
--              Object Name  :
--                File Name  :  adc_current_conversion.vhd
--            Function List  :  None
--                   Author  :  Mark Bonning 
--                     Date  :  07/05/2019
--                                                                          
-- -------------------------------------------------------------------------------
-- Description
-- Converts adc value to Current in mA. Also give a votlage output in mV
-- -------------------------------------------------------------------------------
-- History
-- Issue 0.01: Mark Bonning 
-- -------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity adc_current_conversion is
    port (


	  	adc_value_i							: in std_logic_vector(11 downto 0);		-- adc value 
	  	adc_offset_value_i				: in std_logic_vector(11 downto 0);		-- adc value from the offset monitoring adc
		
		current_o							: out std_logic_vector(7 downto 0);		-- current conversion in mA
		
		voltage_o							: out std_logic_vector(23 downto 0)		-- test port only, voltage at the adc in mV
		  
    );
end entity adc_current_conversion;

architecture imp_adc_current_conversion of adc_current_conversion is

	-- local signals
	constant	uV_per_bit					: unsigned(11 downto 0) := x"2dc";		-- 3V reference so in uV, 3,000,000 / 4095 = 732 decimal 

	signal		signal_uV_s				: unsigned(23 downto 0);					-- the signal value in microvolts
	signal		offset_uV_s				: unsigned(23 downto 0);					-- the offset value in microvolts
	
	signal		V_uV_s						: unsigned(23 downto 0);					-- calculated volatge across the resistor in microvolts
	signal		I_mA_s						: unsigned(23 downto 0);					-- calculated current in milliamps
	
	signal		offset_removed_uV_s		:unsigned(23 downto 0);						-- signal to hold the voltage with the offset removed in micovolts

begin

-- logic
 
signal_uV_s <= unsigned(adc_value_i) * uV_per_bit;
offset_uV_s <= unsigned(adc_offset_value_i) * uV_per_bit;

offset_removed_uV_s <= (signal_uV_s - offset_uV_s) / 10 when signal_uV_s > offset_uV_s else (others => '0');
V_uV_s <= offset_uV_s - offset_removed_uV_s when offset_removed_uV_s < offset_uV_s else (others => '0');
I_mA_s <= V_uV_s / 2200;

-- current across resistor in mA
current_o <= std_logic_vector(I_mA_s(7 downto 0));

--voltage_o <= std_logic_vector(V_uV_s / 1000);

-- voltage at adc in mV
voltage_o <= std_logic_vector((unsigned(adc_value_i) * uV_per_bit) / 1000);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

end architecture imp_adc_current_conversion;

